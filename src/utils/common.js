const convertToYuan = (price) => {
   return (price * 0.01).toFixed(2)
}

export default {
  convertToYuan
}

