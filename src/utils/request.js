import Request from 'luch-request'
import getMyUrl from '@/config'
export const apiBaseUrl = getMyUrl.apiBaseUrl

const api = new Request()
const token = uni.getStorageSync('token')

// 认证头
const authToken = {
  'Authorization': 'Bearer ' + token,
}

// 在這裡自定义请api的求头 ，有一些固定的參數再加上token
api.config.header = {
  "Accept": "application/json",
  "Content-Type": "application/json; charset=UTF-8",
  "X-Requested-With": "XMLHttpRequest",
  ...authToken //token 的解構
}
// 發送請求，最終是要把請求頭的信息放到請求中。
api.interceptors.request.use((config) => { // 可使用async await 做异步操作
  config.baseURL = apiBaseUrl
  config.header = {
    ...api.config.header, // 這是luch-request ，request的配置文件config，把他展開到uni-app裡面的request的config裡面
  }
  //console.log( config) //暫時不理解，這個可能是uni-app 的config請求配置
  return config
}, config => { // 可使用async await 做异步操作
  return Promise.reject(config)
})
//響應回來，這裡可以獲取到他的錯誤或者正確的信息，可以獲取statusCode，狀態碼
api.interceptors.response.use((response) => {
  // console.log(response) // 響應狀態的對象
  //  console.log(response.statusCode) 响应请求的状态码，成功或者失敗
  return response
}, (response) => {//出現錯誤的時候執行這個函數

  console.log('error')
  console.log(response)
  console.log(response.statusCode)

  return Promise.reject(response)
})

/**
 * 普通的http请求
 * @type {Request}
 */
const http = new Request()
http.config.header = {} //普通请求无头
http.config.baseURL = apiBaseUrl
http.interceptors.request.use((config) => { // 可使用async await 做异步操作
  config.header = {
    ...config.header
  }
  return config
}, config => { // 可使用async await 做异步操作
  return Promise.reject(config)
})

export {
  http,
  api
}
