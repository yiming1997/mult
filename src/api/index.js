import {http, api} from '@/utils/request'
import config from '@/config'

const getToken = () => {
  uni.login({
    success: async function success(res) {
      // step.1 jsCode换openId
      const code2SessionRes = await code2Session({
        code: res.code,
        clientId: config.CLIENT_ID
      })
      const openId = code2SessionRes.data.data
      console.log(openId)
      // step.2 openId换token
      const openId2TokenRes = await openId2Token({
        openId,
        grantType: 'social',
        clientId: config.CLIENT_ID,
        clientSecret: config.CLIENT_SECRET
      })
      const token = openId2TokenRes.data.data.access_token
      // step.3 保存token
      uni.setStorageSync('token', token)
      console.log(token)
    },
    fail: function fail() {
      // console.log('請求失敗')
    },
    complete: function complete() {
      // console.log('請求完成咯')
    }
  })
}

const code2Session = data => http.post(
  config.API_BASE_URL + '/auth/login/code2Session',
  data
)
export const baseImgUrl = config.baseImgUrl

const openId2Token = data => http.post(
  config.API_BASE_URL + '/auth/login/open_id/token',
  data
)

const getSwiperList = data => api.post(
  config.API_BASE_URL + '/applet/slideshow/portal',
  data)

const getNavList = businessId => api.get(
  config.API_BASE_URL + `/applet/navigation/list/${businessId}`
)

const getPersonalMessge = () => api.get(
  config.API_BASE_URL + '/applet/user'
)

const getLiveSwiperList = data => api.post(
  config.API_BASE_URL + '/applet/slideshow/list',
  data
)

const getNaviList = businessId => api.get(    //get请求传参写法
  config.API_BASE_URL + `/applet/navigation/list/${businessId}`
)

const getGoodsList = data => api.post(
  config.API_BASE_URL + '/applet/goods/item/page',
  data
)

const getLiveGoodsList = data => api.post(
  config.API_BASE_URL + '/applet/live/page',
  data
)


const getCategoryGoodsList = () => api.get(
  config.API_BASE_URL + `/applet/category/tree`
)

const imageViewUrl = config.API_BASE_URL + '/storage/view/'

export default {
  getSwiperList,
  getLiveSwiperList,
  getToken,
  imageViewUrl,
  getNaviList,
  getGoodsList,
  getCategoryGoodsList,
  getPersonalMessge,
  getNavList,
  getLiveGoodsList
}
