import Vue from 'vue'
import App from './App'
import { http, api} from './utils/request'
// uView
import uView from 'uview-ui'
Vue.use(uView)

// vuex
import store from '@/store'

// 引入uView提供的对vuex的简写法文件
let vuexStore = require('@/store/$u.mixin.js')
Vue.mixin(vuexStore)

Vue.config.productionTip = false

App.mpType = 'app'

Vue.prototype.$http = http
Vue.prototype.$api = api

const app = new Vue({
  store,
  ...App
})
app.$mount()
